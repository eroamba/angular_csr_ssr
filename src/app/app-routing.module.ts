import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { ShopComponent } from './pages/shop/shop.component';
import { TestimonialComponent } from './pages/testimonial/testimonial.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'testimonial', component: TestimonialComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'shop', component: ShopComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
